﻿using Microsoft.VisualBasic.CompilerServices;
using System;

namespace CollectionChaosArray
{
    class Program
    {
        static object GetInput()
        {
            string text = Console.ReadLine().ToLower();
            if (Int32.TryParse(text, out int number))
            {
                return number;
            }
            else
            {
                return text;
            }
        }

        static void Main(string[] args)
        {
            ChaosArray<object> collection = new ChaosArray<object>();
            bool completed = false;

            Console.WriteLine("Welcome!");
            while (!completed)
            {

                try
                {
                    Console.WriteLine("Do you want to add, remove, get from the list? please type add, remove or get, you can type NO to end program.");
                    string textToCheck = Console.ReadLine().ToLower();

                    if (textToCheck == "no")
                    {
                        completed = true;
                    }
                    else if (textToCheck == "remove")
                    {
                        collection.Remove();
                    }
                    else if (textToCheck == "get")
                    {
                        collection.Get();
                    }
                    else if (textToCheck == "add")
                    {
                        Console.WriteLine("you can add numbers or text.");
                        collection.Add(GetInput());
                    }
                    else
                    {

                    }
                }

                catch (ArgumentException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            Console.WriteLine(collection.Count());
            collection.GetCollection();

        }
    }
}
