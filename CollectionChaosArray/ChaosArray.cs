﻿using System;
using System.Collections;
using System.Reflection;

namespace CollectionChaosArray
{
    public class ChaosArray<T>
    {
        object[] ChaosList = new object[100];
        // static ArrayList ChaosList = new ArrayList();

        Random RandomNumber = new Random();

        public void Add(T item)
        {
            int random = RandomNumber.Next(100);
            if (ChaosList[random] == null)
            {
                ChaosList[random] = item;
            }
            else
            {
                throw new ArgumentException("Adding to a field already occupied.");
            }
        }

        public void Remove()
        {
            int random = RandomNumber.Next(100);
            if (ChaosList[random] == null)
            {
                throw new ArgumentException("Trying to remove something that is not there.");
            }
            else
            {
                ChaosList[random] = null;
                Console.WriteLine("Removing successfull");
            }
        }

        public int Count()
        {
            return ChaosList.Length;
        }

        public object Get()
        {
            int random = RandomNumber.Next(100);

            if (ChaosList[random] != null)
            {
                // for testing: Console.WriteLine(ChaosList[random]);
                return ChaosList[random];
            }
            else
            {
                throw new ArgumentException("Trying to get an item that is not there.");
            }
        }
        public void GetCollection()
        {
            foreach (T item in ChaosList)
            {
                Console.WriteLine(item + ", ");
            }
        }
    }
}
